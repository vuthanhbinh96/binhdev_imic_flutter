import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        title: 'Material App',
        theme: ThemeData(
          primarySwatch: Colors.purple,
        ),
        home: const HomeScreen());
  }
}

final products = [
  {
    'id': 'PIM3gUf',
    'title': 'Product 1',
    'description': 'Product Description 1',
    'price': 10.5,
    'quantity': 1,
    'imageUrl': 'https://picsum.photos/714/327?random=1'
  },
  {
    'id': 'KB33SybV5',
    'title': 'Product 2',
    'description': 'Product Description 2',
    'price': 10.5,
    'quantity': 1,
    'imageUrl': 'https://picsum.photos/714/327?random=2'
  },
  {
    'id': 'rTuoFc4XZQIjbC',
    'title': 'Product 3',
    'description': 'Product Description 3',
    'price': 10.5,
    'quantity': 1,
    'imageUrl': 'https://picsum.photos/714/327?random=3'
  },
  {
    'id': 'DgCmGU8ZtyXSahQqHcB1',
    'title': 'Product 3',
    'description': 'Product Description 4',
    'price': 10.5,
    'quantity': 1,
    'imageUrl': 'https://picsum.photos/714/327?random=4'
  },
];

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: const <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text(
                'Drawer Header',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.message),
              title: Text('Messages'),
            ),
            ListTile(
              leading: Icon(Icons.account_circle),
              title: Text('Profile'),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: [
          IconButton(
              onPressed: () async {
                final product =
                    await Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => EditProduct(),
                ));
                if (product != null) {
                  products.add(product);
                  setState(() {});
                }
              },
              icon: const Icon(Icons.add))
        ],
      ),
      body: ListView.separated(
          itemBuilder: (context, index) {
            final product = products[index];
            return ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage('${product['imageUrl']}'),
              ),
              title: Text('${product['title']}'),
              subtitle: Text('${product['description']}'),
              trailing: Wrap(children: [
                IconButton(
                  onPressed: () async {
                      final editProduct =
                          await Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => EditProduct(product: product),
                      ));
                      if (editProduct != null) {
                        products
                            .firstWhere(
                                (element) => element['id'] == editProduct['id'])
                            .addAll({...editProduct});
                        setState(() {});
                      }
                    },
                  icon: const Icon(Icons.edit),
                  color: Colors.purple,
                ),
                IconButton(
                  onPressed: () {
                      showDialog(
                        context: context,
                        builder: (dialogContext) => AlertDialog(
                          title: const Text('Are u sure?'),
                          content: const Text('Are u sure u want to delete this product?'),
                          actions: [
                            ElevatedButton(
                                onPressed: () {
                                  products.removeWhere((element) =>
                                      element['id'] == product['id']);
                                  setState(() {});
                                  Navigator.pop(dialogContext);
                                },
                                child: const Text('Ok')),
                            TextButton(
                                onPressed: () {
                                  Navigator.pop(dialogContext);
                                },
                                child: const Text('Cancel'))
                          ],
                        ),
                      );
                    },
                  icon: const Icon(Icons.delete),
                  color: Colors.red,
                ),
              ]),
            );
          },
          separatorBuilder: ((context, index) => const Divider()),
          itemCount: products.length),
    );
  }
}

class EditProduct extends StatefulWidget {
  final Map<String, Object>? product;
  const EditProduct({super.key, this.product});

  @override
  State<EditProduct> createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  final _formKey = GlobalKey<FormState>();
  final _product = <String, Object>{};

  @override
  void initState() {
    _product.addAll({...widget.product ?? {}});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Products'),
        actions: [
          IconButton(
              onPressed: () {
                if (_formKey.currentState?.validate() == true) {
                  _formKey.currentState?.save();
                  print(_product);
                  Navigator.of(context).pop(_product);
                }
              },
              icon: const Icon(Icons.save))
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
          children: [
          TextFormField(
            initialValue: '${_product['title'] ?? ''}',
            decoration: const InputDecoration(
              label: Text('Title'),
            ),
            validator: (value) {
              if (value?.isEmpty == true) {
                return 'Title cannot be empty';
              }
              return null;
            },
            onSaved: (newValue) {
              _product['title'] = newValue ?? '';
            },
          ),
          TextFormField(
            initialValue: '${_product['price'] ?? ''}',
            decoration: const InputDecoration(
              label: Text('Price'),
            ),
            validator: (value) {
              if (value == null || value.isEmpty == true) {
                return 'Price cannot be empty';
              }
              if (double.tryParse(value) == null) {
                return 'Price cannot be empty';
              }
              return null;
            },
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            keyboardType: TextInputType.number,
            onSaved: (newValue) {
              _product['price'] = newValue ?? '';
            },
          ),
          TextFormField(
            initialValue: '${_product['description'] ?? ''}',
            decoration: const InputDecoration(
              label: Text('Description'),
            ),
            validator: (value) {
              if (value?.isEmpty == true) {
                return 'Description cannot be empty';
              }
              return null;
            },
            onSaved: (newValue) {
              _product['description'] = newValue ?? '';
            },
          ),
          TextFormField(
            initialValue: '${_product['imageUrl'] ?? ''}',
            decoration: const InputDecoration(
              label: Text('Image Url'),
            ),
            validator: (value) {
              if (value?.isEmpty == true) {
                return 'Image cannot be empty';
              }
              return null;
            },
            onSaved: (newValue) {
              _product['imageUrl'] = newValue ?? '';
            },
          ),
        ],
      )),
      ) 
    );
  }
}
