import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Material App',
      home: Scaffold(
        body:  AuthScreen(),
      ),
    );
  }
}

enum FormType{
  Login,
  Register,
}

class AuthScreen extends StatelessWidget {
  const AuthScreen({super.key});
  

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(colors: [Colors.pink, Colors.purple])),
        ),
        Column(
          children: [
            const Spacer(),
            Transform.rotate(
              angle: -3.14 / 12,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                decoration: const BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: const Text(
                  'MyShop',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            const SizedBox(height: 10,),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              padding: const EdgeInsets.all(10),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(15)),
              ),
              child: Column(
                children:  [
                  const TextField(
                    decoration: InputDecoration(
                        label: Text('E-mail'),
                        prefixIcon: Icon(Icons.email_rounded)),
                  ),
                  const TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                        label: Text('Password'),
                        prefixIcon: Icon(Icons.email_rounded)),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 20)),
                    child: const Text('Login'),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextButton(onPressed: () {}, child: const Text('Register')),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            const Spacer(),
          ],
        )
      ],
    );
  }
}
